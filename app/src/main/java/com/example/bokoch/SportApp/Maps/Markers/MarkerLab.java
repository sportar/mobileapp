package com.example.bokoch.SportApp.Maps.Markers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.bokoch.SportApp.DataBase.DataBase;
import com.example.bokoch.SportApp.DataBase.DataBaseCursorWrapper;
import com.example.bokoch.SportApp.DataBase.DbSchema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MarkerLab {

    private static MarkerLab sMarkersLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DataBase mBaseHelper;

    public void addMarkers(MarkerMaps c) {
        ContentValues values = getContentValues(c);
    }

    public void updateMarkers(MarkerMaps marker) {
        String uuidString = String.valueOf(marker.getId());
        ContentValues values = getContentValues(marker);

        mDatabase.update(DbSchema.MarkersTable.NAME, values,
                DbSchema.MarkersTable.Cols.ID + " = ?",
                new String[] {uuidString});

    }

    private DataBaseCursorWrapper queryMarkers(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                DbSchema.MarkersTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new DataBaseCursorWrapper(cursor);
    }

    public static MarkerLab get(Context context) {
        if (sMarkersLab == null) {
            sMarkersLab = new MarkerLab(context);
        }
        return sMarkersLab;
    }

    private MarkerLab(Context context) {
        mContext = context.getApplicationContext();
        mBaseHelper = new DataBase(context);
        try {
            mBaseHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDatabase = mBaseHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }

    public List<MarkerMaps> getMarkers() {
        List<MarkerMaps> markers = new ArrayList<>();
        //ОШИБКА, братишка
        DataBaseCursorWrapper cursor = queryMarkers(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                markers.add(cursor.getMarkerMaps());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return markers;
    }
    private static ContentValues getContentValues(MarkerMaps marker) {
        ContentValues values = new ContentValues();
        values.put(DbSchema.MarkersTable.Cols.ID, marker.getId());
        values.put(DbSchema.MarkersTable.Cols.NAME_PLACES, marker.getNamePlaces());
        values.put(DbSchema.MarkersTable.Cols.LAT, marker.getPosition().latitude);
        values.put(DbSchema.MarkersTable.Cols.LNG, marker.getPosition().longitude);
        return values;
    }
    public MarkerMaps getMarker(int id) {
        Log.i("getMarkerMaps", String.valueOf(id));
        DataBaseCursorWrapper cursor = queryMarkers(DbSchema.MarkersTable.Cols.ID + " = ?",
                new String[] { String.valueOf(id)});
        try {
            if (cursor.getCount() == 0) {
                Log.i("getMarkerMaps", "getCount == 0");
                return null;
            }
            cursor.moveToFirst();
            return cursor.getMarkerMaps();
        } finally {
            cursor.close();
        }
    }

}
