package com.example.bokoch.SportApp.Training;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.bokoch.SportApp.DataBase.JsonParser;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.Training.Utils.WorkoutsRequest;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FragmentWorkoutsListRecyclerView extends Fragment {

    private WorkoutAdapter mAdapter;
    List<Workout> mWorkouts;
    // Фабрика для создания фрагмента
    public static FragmentWorkoutsListRecyclerView newInstance() {
        FragmentWorkoutsListRecyclerView fragment = new FragmentWorkoutsListRecyclerView();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.workout_card_view, container, false);

        mWorkouts = new ArrayList<>();
        try {
            Log.d("Сука сука сука", "cyka");
            mWorkouts = JsonParser.getWorkoutFromJSON(WorkoutsRequest.WORKOUTS_EXAMPLES);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_view_workout);

        mAdapter = new WorkoutAdapter(getContext(), mWorkouts);


        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @UiThread
            @Override
            public void onParentExpanded(int parentPosition) {
                Workout expandedWorkout = mWorkouts.get(parentPosition);
                Log.i("Recipe expanded clicked", expandedWorkout.getDateString());
            }

            @UiThread
            @Override
            public void onParentCollapsed(int parentPosition) {
                Workout collapsedWorkouts = mWorkouts.get(parentPosition);
                Log.i("Recipe collapsed", collapsedWorkouts.getDateString());
            }
        });
        Log.d("FRAGMENT", "before setAdapter");

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("FRAGMENT", "onSaveInstance");
        mAdapter.onSaveInstanceState(outState);
    }

   /* @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }*/
}