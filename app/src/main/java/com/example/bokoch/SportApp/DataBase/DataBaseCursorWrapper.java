package com.example.bokoch.SportApp.DataBase;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import com.example.bokoch.SportApp.Exercises.Exercise;
import com.example.bokoch.SportApp.Maps.Markers.MarkerMaps;
import com.example.bokoch.SportApp.Training.Workout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DataBaseCursorWrapper extends CursorWrapper {
    public DataBaseCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Exercise getExercise() {

        int uuidInt = getInt(getColumnIndex(DbSchema.ExerciseTable.Cols.UUID));
        String description = getString(getColumnIndex(DbSchema.ExerciseTable.Cols.DESCRIPTION));
        String type = getString(getColumnIndex(DbSchema.ExerciseTable.Cols.TYPE));
        String name = getString(getColumnIndex(DbSchema.ExerciseTable.Cols.NAME_EXERCISE));
        int complexity = getInt(getColumnIndex(DbSchema.ExerciseTable.Cols.COMPLEXITY));
       // byte[] imageBlob = getBlob(4);
        Exercise exercise = new Exercise(uuidInt, name, type, complexity, description /*,
                BitmapFactory.decodeByteArray(imageBlob, 0, imageBlob.length)*/);
        return exercise;
    }

    public MarkerMaps getMarkerMaps() {
        int id = getInt(getColumnIndex(DbSchema.MarkersTable.Cols.ID));
        String namePlaces = getString(getColumnIndex(DbSchema.MarkersTable.Cols.NAME_PLACES));
        double lat = getDouble(getColumnIndex(DbSchema.MarkersTable.Cols.LAT));
        double lng = getDouble(getColumnIndex(DbSchema.MarkersTable.Cols.LNG));

        MarkerMaps marker = new MarkerMaps(id, namePlaces, lat, lng);
        return marker;
    }
    /*public Workout getWorkout() throws ParseException {
        int id = getInt(getColumnIndex(DbSchema.WorkoutTable.Cols.ID));
        String date = getString(getColumnIndex(DbSchema.WorkoutTable.Cols.DATE_TRAINING));
        String jsonWorkoutText = getString(getColumnIndex(DbSchema.WorkoutTable.Cols.LIST_EXERCISE));


        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date mDate = format.parse("22.08.1998");
        return new Workout(id, mDate, jsonWorkoutText);
    }*/
}
