package com.example.bokoch.SportApp.Training;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.bokoch.SportApp.Exercises.Exercise;

import java.util.ArrayList;
import java.util.List;


//Упражнение
public class WorkoutExercise implements Parcelable {

    //Подходы
    private String[] mSets;

    //Повторения
    private int mWeight;

    private String mName;

    private int mId;

    protected WorkoutExercise(Parcel in) {
        mWeight = in.readInt();
        mName = in.readString();
        mId = in.readInt();
    }

    public static final Creator<WorkoutExercise> CREATOR = new Creator<WorkoutExercise>() {
        @Override
        public WorkoutExercise createFromParcel(Parcel in) {
            return new WorkoutExercise(in);
        }

        @Override
        public WorkoutExercise[] newArray(int size) {
            return new WorkoutExercise[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getWeight() {
        return mWeight;
    }

    public void setWeight(int weight) {
        mWeight = weight;
    }

    public String[] getSets() {
        return mSets;
    }

    public void setSets(String[] sets) {
        mSets = sets;
    }

    public WorkoutExercise(int id, String name, int weight, String[] sets) {
        mId = id;
        mSets = sets;
        mName = name;
        mWeight = weight;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mWeight);
        dest.writeString(mName);
        dest.writeInt(mId);
        dest.writeStringArray(mSets);
    }
}
