package com.example.bokoch.SportApp.Maps.Markers;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.example.bokoch.SportApp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * Created by bokoch on 3/15/2018.
 */

public class MarkerRenderer extends DefaultClusterRenderer<MarkerMaps> {
    private Context context;
    private int pinWith = 100;
    private int pinHeight = 110;

    public MarkerRenderer(Context context, ClusterManager<MarkerMaps> clusterManager, GoogleMap map) {
        super(context, map, clusterManager);
        this.context = context;
    }

    @Override
    protected void onBeforeClusterItemRendered(MarkerMaps myItem, MarkerOptions markerOptions) {
        // Customize the marker here
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MarkerMaps> cluster, MarkerOptions markerOptions)
    {
        // Customize the cluster here
        IconGenerator iconGenerator = new IconGenerator(context.getApplicationContext());
        iconGenerator.setBackground(ContextCompat.getDrawable(context, R.drawable.m3));

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View iconView = inflater.inflate(R.layout.map_cluster_view, null, false);
//        iconGenerator.setContentView(iconView);

        Bitmap bg = iconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bg, pinWith, pinHeight, false);

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
    }
}