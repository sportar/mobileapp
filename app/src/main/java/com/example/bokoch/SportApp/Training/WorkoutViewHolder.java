package com.example.bokoch.SportApp.Training;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.bokoch.SportApp.R;



public class WorkoutViewHolder  extends ParentViewHolder {


    private TextView mWorkoutTextView;
    private TextView mWorkoutDate;
    private TextView mWorkoutTime;

    public WorkoutViewHolder(@NonNull View itemView) {
        super(itemView);
        Log.d("Workout holder", "Init");

        mWorkoutTextView = (TextView) itemView.findViewById(R.id.text_name_workout_training);
        mWorkoutDate = (TextView) itemView.findViewById(R.id.text_date_workout);
        mWorkoutTime = (TextView) itemView.findViewById(R.id.text_time_workout);
    }

    public void bind(@NonNull Workout workout) {
        Log.d("Workout holder", "SetText");

        mWorkoutTextView.setText(workout.getDay());
        mWorkoutDate.setText(workout.getDateDDMMYYYString());
        mWorkoutTime.setText(workout.getDateHHSSString());
    }

}