package com.example.bokoch.SportApp.LoginActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bokoch.SportApp.Menu.MenuActivity;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.User.User;
import com.example.bokoch.SportApp.User.UserUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;

import es.dmoral.toasty.Toasty;


public class SignUpActivity extends Activity implements View.OnFocusChangeListener{
    Button mSignInButton;
    EditText mLoginEditText;
    EditText mPasswordEditText;
    EditText mMailEditText;
    TextInputLayout mUsernameLayout;
    private ProgressDialog pDialog;
    private SessionManager mSession;
    AVLoadingIndicatorView mRunLoad;
    User mUser;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.auth_sign_up);

        mUser = new User();

        mLoginEditText = (EditText) findViewById(R.id.registration_login_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.registration_password_edit_text);
        mMailEditText = (EditText) findViewById(R.id.registration_mail_edit_text);

        mLoginEditText.setHintTextColor(R.color.colorBlack);
        mPasswordEditText.setHintTextColor(R.color.colorBlack);
        mMailEditText.setHintTextColor(R.color.colorBlack);

        mLoginEditText.setOnFocusChangeListener(this);
        mPasswordEditText.setOnFocusChangeListener(this);

        mRunLoad = (AVLoadingIndicatorView) findViewById(R.id.avloading_indicator_sign_up);

        mRunLoad.hide();
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        mSession = new SessionManager(getApplicationContext());

        /*// Check if user is already logged in or not
        if (mSession.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(SignUpActivity.this,
                                                            MenuActivity.class);
            startActivity(intent);
            finish();
        }*/

        mSignInButton = (Button)findViewById(R.id.registration_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRunLoad.show();
                mUser.setLoginText(mLoginEditText.getText().toString());
                mUser.setPasswordText(mPasswordEditText.getText().toString());
                mUser.setMailText(mMailEditText.getText().toString());

                if (!mUser.getLoginText().isEmpty() && !mUser.getPasswordText().isEmpty()
                      &&  !mUser.getMailText().isEmpty()) {

                    RegistrationAsyncTask task = new RegistrationAsyncTask();
                    task.execute();

                    try {
                        mUser = task.get();
                        switch (mUser.getValid()) {
                            case UserUtils.Login.OK_VALID_USER:
                                Toasty.success(getApplicationContext(), "Success!", Toast.LENGTH_SHORT, true).show();
                                mSession.setLogin(mUser);
                                Intent intent = new Intent(SignUpActivity.this, MenuActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case UserUtils.Login.NO_VALID_USER:
                                Toasty.warning(getApplicationContext(), "No. Your Login or e-mail uncorrect", Toast.LENGTH_SHORT, true).show();
                                break;
                            case UserUtils.Login.VALID_USER_EXIST:
                                Toasty.info(getApplicationContext(), "Sorry user exist!", Toast.LENGTH_SHORT, true).show();
                                break;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toasty.warning(getApplicationContext(), "Error, your login or password is empty!", Toast.LENGTH_SHORT, true).show();
                }
                mRunLoad.hide();
            }
        });

    }

    private class RegistrationAsyncTask extends AsyncTask<URL, Void, User> {
        @Override
        protected User doInBackground(URL... urls) {
            // Create URL object
            URL url = HttpRequest.createUrl(UserUtils.Login.URL + "/"
                    + mUser.getLoginText() + "/" + mUser.getPasswordText() + "/"
            + mUser.getMailText());

            // Perform HTTP request to the URL and receive a JSON response back
            String jsonResponse = "";
            try {
                jsonResponse = HttpRequest.makeHttpRequest(url, "POST");
            } catch (IOException e) {
                Log.d("AsyncTask", "Response error");
            }
            Log.d("URL", jsonResponse);

            if (jsonResponse.equals("success")) {
                mUser.setValid(UserUtils.Login.OK_VALID_USER);
            } else {
                if (jsonResponse.equals("user_exist")) {
                    mUser.setValid(UserUtils.Login.VALID_USER_EXIST);
                } else {
                    mUser.setValid(UserUtils.Login.NO_VALID_USER);
                }
            }
            // Return the {@link Event} object as the result fo the {@link TsunamiAsyncTask}
            return mUser;
        }



        /**
         * Returns new URL object from the given string URL.
         */
        private URL createUrl(String stringUrl) {
            URL url = null;
            try {
                url = new URL(stringUrl);
            } catch (MalformedURLException exception) {
                Log.e("create Url", "Error with creating URL", exception);
                return null;
            }
            return url;
        }

        /**
         * Make an HTTP request to the given URL and return a String as the response.
         */
        private String makeHttpRequest(URL url, String method) throws IOException {
            String jsonResponse = "";
            HttpURLConnection urlConnection = null;
            InputStream inputStream = null;
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod(method);
                urlConnection.setReadTimeout(10000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.connect();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = urlConnection.getInputStream();
                    jsonResponse = readFromStream(inputStream);
                } else {
                    Log.e("make HTTP request", "Error response code: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {
                Log.e("make HTTP request", "makeHttpRequest", e);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (inputStream != null) {
                    // function must handle java.io.IOException here
                    inputStream.close();
                }
            }
            return jsonResponse;
        }

        /**
         * Convert the {@link InputStream} into a String which contains the
         * whole JSON response from the server.
         */
        private String readFromStream(InputStream inputStream) throws IOException {
            StringBuilder output = new StringBuilder();
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String line = reader.readLine();
                while (line != null) {
                    output.append(line);
                    line = reader.readLine();
                }
            }
            return output.toString();
        }

    }
    @SuppressLint({"ResourceAsColor", "ResourceType"})
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        /*if (v == mLoginEditText) {
            mLoginEditText.setHintTextColor(R.color.colorBlack);
        } else {
            if (v != mPasswordEditText) {
                mPasswordEditText.setHintTextColor(R.color.colorBlack);
            } else {
                if (v != mLoginEditText && v)
            }
        }*/
    }
}
