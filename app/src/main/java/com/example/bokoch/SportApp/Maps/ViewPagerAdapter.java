package com.example.bokoch.SportApp.Maps;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.bokoch.SportApp.Maps.Markers.MarkerMaps;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<MarkerMaps> mMarkers;

    public ViewPagerAdapter(FragmentManager fm, List<MarkerMaps> markers) {
        super(fm);
        mMarkers = markers;
    }

//    public ViewPagerAdapter(FragmentManager fm) {
//        super(fm);
//    }

    @Override
    public int getCount() {
        Log.d("getCount", String.valueOf(mMarkers.size()));
        return mMarkers.size();
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        args.putDouble(MarkerFragment.ARG_MARKER_DISTANCE, mMarkers.get(position).getDistance());
        args.putString(MarkerFragment.ARG_MARKER_NAME, mMarkers.get(position).getNamePlaces());
        return MarkerFragment.newInstance(args);
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//
//        return "Child Fragment " + position;
//    }

}