package com.example.bokoch.SportApp.LoginActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.bokoch.SportApp.User.User;
import com.google.gson.Gson;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences pref;

    private SharedPreferences.Editor editor;
    private Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "UserLogin";

    private static final String KEY_IS_LOGIN = "User";

    public SessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(User user) {
        editor.putString(KEY_IS_LOGIN, new Gson().toJson(user));
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public void removeLogin() {
        editor.remove(KEY_IS_LOGIN).commit();
        editor.remove(KEY_IS_LOGIN);
        editor.apply();
    }
    public User getUser() {
        String json = pref.getString(KEY_IS_LOGIN, "");
        return new Gson().fromJson(json, User.class);
    }

    public boolean isLoggedIn(){
        Log.d(TAG, "IsLoggedIN " );

        return !pref.getString(KEY_IS_LOGIN, "").equals("");
    }
}
