package com.example.bokoch.SportApp.Training;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.bokoch.SportApp.R;

import java.util.List;

public class WorkoutAdapter extends ExpandableRecyclerAdapter<Workout, WorkoutExercise, WorkoutViewHolder,
        WorkoutExerciseViewHolder> {

    private static final int PARENT_NORMAL = 1;
    private static final int CHILD_NORMAL = 2;

    private LayoutInflater mInflater;
    private List<Workout> mWorkouts;

    public WorkoutAdapter(Context context, @NonNull List<Workout> workouts) {
        super( workouts);
        Log.d("Workout ADAPTER", "Init");

        mWorkouts = workouts;
        mInflater = LayoutInflater.from(context);
        Log.d("Workout ADAPTER", "Init");

    }

    @UiThread
    @NonNull
    @Override
    public WorkoutViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int i) {
        View workoutView;
        Log.d("Workout ADAPTER", "onCreateParentViewHolder");

        workoutView = mInflater.inflate(R.layout.workout_card_view_item, parentViewGroup, false);
        Log.d("Workout ADAPTER", "onCreateParentViewHolder2");

        return new WorkoutViewHolder(workoutView);
    }

    @UiThread
    @NonNull
    @Override
    public WorkoutExerciseViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View workoutExerciseView ;
        Log.d("Workout ADAPTER", "onCreateChildViewHolder");

        workoutExerciseView = mInflater.inflate(R.layout.workout_exercise_item, childViewGroup, false);
        Log.d("Workout ADAPTER", "onCreateChildViewHolder2");

        return new WorkoutExerciseViewHolder(workoutExerciseView);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull WorkoutViewHolder workoutViewHolder, int parentPosition, @NonNull Workout workout) {
        Log.d("Workout ADAPTER", "onBindParentViewHolder");

        workoutViewHolder.bind(workout);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull WorkoutExerciseViewHolder workoutExerciseViewHolder,
                                      int parentPosition, int childPosition, @NonNull WorkoutExercise workoutExercise) {
        Log.d("Workout ADAPTER", "onBindChildViewHolder");

        workoutExerciseViewHolder.bind(workoutExercise);
    }

    @Override
    public int getParentViewType(int parentPosition) {
        Log.d("Workout ADAPTER", "getParentViewType");

        return PARENT_NORMAL;
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        Log.d("Workout ADAPTER", "getChildViewType");

        return CHILD_NORMAL;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        Log.d("Workout ADAPTER", "isParentViewType");

        return viewType == PARENT_NORMAL;
    }

}