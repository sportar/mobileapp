package com.example.bokoch.SportApp.Training;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.bokoch.SportApp.Exercises.Exercise;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

//Список упражнений для одной тренировки
public class Workout  implements Parent<WorkoutExercise> {

    private int mId;
    private Date mDate;
    private List<WorkoutExercise> mTraining;


    public Workout(int id, Date date, List<WorkoutExercise> listExercise) {
        mId = id;
        mDate = date;
        mTraining = listExercise;
    }
    public List<WorkoutExercise> getTraining() {
        return mTraining;
    }

    public String getDateString() {
        return mDate.toString();
    }

    public void setTraining(List<WorkoutExercise> training) {
        mTraining = training;
    }
    @Override
    public List<WorkoutExercise> getChildList() {
        return mTraining;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getDay() {
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
        return dayFormat.format(mDate);
    }
    public String getDateDDMMYYYString() {
        SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return dayFormat.format(mDate);
    }
    public String getDateHHSSString() {
        SimpleDateFormat dayFormat = new SimpleDateFormat("HH:SS", Locale.US);
        return dayFormat.format(mDate);
    }

}
