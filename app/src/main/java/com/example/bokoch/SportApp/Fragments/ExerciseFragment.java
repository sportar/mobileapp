package com.example.bokoch.SportApp.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bokoch.SportApp.Exercises.Exercise;
import com.example.bokoch.SportApp.Exercises.ExerciseLab;
import com.example.bokoch.SportApp.R;

import java.util.UUID;

public class ExerciseFragment extends Fragment {
    private static final String ARG_EXERCISE_ID = "exercise_id";

    private Exercise mExercise;
    private TextView mExerciseName;
    private TextView mExerciseType;
    private TextView mExerciseDescription;
    private ImageView mExerciseimageView;
    private Button mExerciseIntoArCore;

    public static ExerciseFragment newInstance(int ExerciseId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_EXERCISE_ID, ExerciseId);

        ExerciseFragment fragment = new ExerciseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int ExerciseId = (int)getArguments().getSerializable(ARG_EXERCISE_ID);
        Log.d("ExerciseOnCreate", String.valueOf(ExerciseId));
        mExercise = ExerciseLab.get(getActivity()).getExercise(ExerciseId);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_exercise, container, false);

        mExerciseName = (TextView)v.findViewById(R.id.fragment_exercise_name_text);
        mExerciseType = (TextView)v.findViewById(R.id.fragment_exercise_type_text);
        mExerciseDescription = (TextView)v.findViewById(R.id.fragment_exercise_description_text);
        mExerciseimageView = (ImageView)v.findViewById(R.id.fragment_exercise_image_view);

        mExerciseName.setText(mExercise.getName());
        mExerciseType.setText(mExercise.getType());

        Log.d("ExerciseFragment", "SetText Type and Name " + mExercise.getName());

        mExerciseDescription.setText(mExercise.getDescription());

        switch (mExercise.getID()) {
            case 1:
                mExerciseimageView.setImageResource(R.drawable.push_up);
                break;
            case 2:
                mExerciseimageView.setImageResource(R.drawable.birpi);
                break;
            case 3:
                mExerciseimageView.setImageResource(R.drawable.prisedania);
                break;
            default:
                mExerciseimageView.setImageResource(R.drawable.birpi);
                Log.i("EveryWheree", String.valueOf(mExercise.getID()));
                break;

        }
//        mExerciseimageView.setImageBitmap(mExercise.getImage());
        return v;
    }
    @Override
    public void onPause() {
        super.onPause();
        ExerciseLab.get(getActivity())
                .updateExercise(mExercise);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
    }
    public void returnResult() {
        getActivity().setResult(Activity.RESULT_OK, null);
    }
}

