package com.example.bokoch.SportApp.Exercises;



import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import com.example.bokoch.SportApp.R;


public class Exercise {
    private int mID;
//    private Bitmap mImage;
    private String mType; // Тип: отжимания, приседания и т.п.
    private String mName; // Имя упражнения
    private int mComplexity; //Сложность
    private String mDescription; // Описание
    public Exercise(int id, String name, String type, int complexity, String description/*, Bitmap image*/) {
        mID = id;
//        mImage = image;
        mComplexity = complexity;
        mDescription = description;
        mName = name;
        mType = type;
    }
    public int getID() {
        return mID;
    }

    public Exercise(int id) {
        mID = id;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getComplexity() {
        return mComplexity;
    }


    public void setComplexity(int complexity) {
        mComplexity = complexity;
    }


    public String getDescription() {
        return mDescription;
    }

    public int getComplexityImage() {
        if (mComplexity == 1) {
            return R.drawable.one_stars;
        } else if (mComplexity == 2){
            return R.drawable.two_stars;
        } else if (mComplexity == 3) {
            return R.drawable.three_stars;
        } else {
            return R.drawable.all_stars;
        }

    }
    /*public void setImage(Bitmap image) {
        mImage = image;
    }
    public Bitmap getImage() {
        return mImage;

    }*/

    public void setDescription(String description) {
        mDescription = description;
    }

}
