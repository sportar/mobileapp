package com.example.bokoch.SportApp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.bokoch.SportApp.Exercises.Exercise;
import com.example.bokoch.SportApp.Exercises.ExerciseLab;
import com.example.bokoch.SportApp.Fragments.ExerciseFragment;

import java.util.List;


public class ExerciseLibraryPagerActivity extends AppCompatActivity {
    private static final String EXTRA_EXERCISE_ID =
            "com.bokoch.android.SportApp.exercise_id";

    private ViewPager mViewPager;
    private List<Exercise> mExercises;

    public static Intent newIntent(Context packageContext, int exerciseId) {
        Intent intent = new Intent(packageContext, ExerciseLibraryPagerActivity.class);
        intent.putExtra(EXTRA_EXERCISE_ID, exerciseId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_exerscise_pager);
        int mExerciseID = (int) getIntent().getSerializableExtra(EXTRA_EXERCISE_ID);

        mViewPager = findViewById(R.id.activity_exercise_view_pager);
        mExercises = ExerciseLab.get(this).getExercises();
        FragmentManager fragmentManager = getSupportFragmentManager();
        //Fragment manager: Что именно делает агент? Вкратце, он добавляет
        // возвращаемые фрагменты в активность и помогает ViewPager
        // идентифицировать представления фрагментов для их правильного размещения
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Exercise exercise = mExercises.get(position);
                return ExerciseFragment.newInstance(exercise.getID());
            }

            @Override
            public int getCount() {
                return mExercises.size();
            }
        });
        for (int i = 0; i < mExercises.size(); i++) {
            if (mExercises.get(i).getID() == mExerciseID) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
