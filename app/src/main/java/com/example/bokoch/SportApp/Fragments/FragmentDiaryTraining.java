package com.example.bokoch.SportApp.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bokoch.SportApp.ExerciseLibraryPagerActivity;
import com.example.bokoch.SportApp.Exercises.Exercise;
import com.example.bokoch.SportApp.Maps.Markers.MarkerLab;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.Training.Utils.WorkoutsRequest;
import com.example.bokoch.SportApp.Training.Workout;
import com.example.bokoch.SportApp.Training.WorkoutLab;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

import static com.example.bokoch.SportApp.DataBase.JsonParser.getWorkoutFromJSON;

/*
Хранится список тренировок. Указана дата и день недели. По нажатию по итему раскрывается список упражнений.
Так же при долгом нажатии можно будет отредактировать тренировку.
 */
public class FragmentDiaryTraining extends Fragment {
    private static final int REQUEST_WORKOUT = 1;

    List<Workout> mWorkouts;
    private RecyclerView mExerciseRecyclerView;
    private WorkoutAdapter mAdapter;

    // Фабрика для создания фрагмента
    public static FragmentDiaryTraining newInstance() {
        FragmentDiaryTraining fragment = new FragmentDiaryTraining();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //Перегрузка списка
    @Override
    public void onResume() {
        super.onResume();
        UpdateUI();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.workout_card_view, container, false);
        mExerciseRecyclerView = (RecyclerView)view.findViewById(R.id.list_view_workout);
        mExerciseRecyclerView.setHasFixedSize(true);
        mExerciseRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        UpdateUI();
        return view;
    }
    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    //обновление фрагмента
    private void UpdateUI() {
        try {
            mWorkouts = getWorkoutFromJSON(WorkoutsRequest.WORKOUTS_EXAMPLES);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("Update", String.valueOf(mWorkouts.size()));
//        WorkoutLab workoutLab = WorkoutLab.get(getActivity());
//        List<Workout> workouts = workoutLab.getWorkouts();
        if (mAdapter == null) {
            mAdapter = new WorkoutAdapter(mWorkouts);

        } else {
            mAdapter.setWorkouts(mWorkouts);
        }
        mAdapter.notifyDataSetChanged();
        mExerciseRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void returnResult() {
        getActivity().setResult(Activity.RESULT_OK, null);
    }

    public class WorkoutHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView mDay;
        private TextView mDate;
        private Workout mWorkout;

        public WorkoutHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mDay = itemView.findViewById(R.id.text_name_workout_training);
            mDate = (TextView) itemView.findViewById(R.id.text_date_workout);
        }
        @Override
        public void onClick(View v) {
//
//            Intent intent = ExerciseLibraryPagerActivity.newIntent(getActivity(), mWorkout.getId());
//            startActivityForResult(intent, REQUEST_WORKOUT);
//
//            Toast.makeText(getActivity(), mWorkout.getId()+" clicked!", Toast.LENGTH_SHORT).show();
        }


        public void bindExercise(Workout workout) {
            Log.i("bindExercise()", workout.getDateString());
            mWorkout = workout;

            mDay.setText(mWorkout.getDateString());
            mDate.setText(String.valueOf(mWorkout.getTraining().size()));
//            mWorkoutText.setText(mWorkout.getTrainingString());

        }
    }

    private class WorkoutAdapter extends RecyclerView.Adapter<WorkoutHolder> {
        private List<Workout> mWorkouts;
        public WorkoutAdapter(List<Workout> workouts) {
            Log.i("workouts", String.valueOf(workouts.size()));
            mWorkouts = workouts;
        }
        @Override
        public WorkoutHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.workout_card_view_item, parent, false);
            return new WorkoutHolder(view);
        }
        public void setWorkouts(List<Workout> workout) {
            mWorkouts = workout;
        }
        @Override
        public void onBindViewHolder(WorkoutHolder holder, int position) {
            Log.i("OnBindViewHolder", "Position = " + position);
            Workout workout = mWorkouts.get(position);
            holder.bindExercise(workout);
        }
        @Override
        public int getItemCount() {
            return mWorkouts.size();
        }

    }
}

