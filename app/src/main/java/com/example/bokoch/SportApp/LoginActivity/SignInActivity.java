package com.example.bokoch.SportApp.LoginActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bokoch.SportApp.Menu.MenuActivity;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.User.User;
import com.example.bokoch.SportApp.User.UserUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import es.dmoral.toasty.Toasty;

import static com.example.bokoch.SportApp.User.UserUtils.Login.NO_VALID_USER;
import static com.example.bokoch.SportApp.User.UserUtils.Login.OK_VALID_USER;
import static com.example.bokoch.SportApp.User.UserUtils.Login.VALID_USER_EXIST;


public class SignInActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private Button mSignInButton;

    private SessionManager mSession;

    private Button mSignUpButton;
    TextInputLayout mUsernameLayout;
    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private User mUser;
    AVLoadingIndicatorView mRunLoad;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_sign_in);

        mUser = new User();

        mUsernameLayout = (TextInputLayout) findViewById(R.id.login_layout);
        mLoginEditText = (EditText) findViewById(R.id.auth_login_edit);
        mPasswordEditText = (EditText) findViewById(R.id.auth_pass_edit);


        mLoginEditText.setHintTextColor(R.color.colorBlack);
        mPasswordEditText.setHintTextColor(R.color.colorBlack);

        mLoginEditText.setOnFocusChangeListener(this);
        mPasswordEditText.setOnFocusChangeListener(this);
        mRunLoad = (AVLoadingIndicatorView) findViewById(R.id.avloading_indicator_sign_in);
        mRunLoad.hide();
        // Session manager
        mSession = new SessionManager(getApplicationContext());
//        Log.d("getSession", mSession.getUser().getLoginText());
        // Check if user is already logged in or not
        if (mSession.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(SignInActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();
        }

        mSignInButton = (Button)findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRunLoad.show();
                mUser.setLoginText(mLoginEditText.getText().toString());
                mUser.setPasswordText(mPasswordEditText.getText().toString());
                // Check for empty data in the form
                if (!mUser.getLoginText().isEmpty() && !mUser.getPasswordText().isEmpty()) {
                    // login user
                    AuthAsyncTask task = new AuthAsyncTask();

                    task.execute();
                    try {
                        mUser = task.get();
                        mSession.setLogin(mUser);
                        switch (mUser.getValid()) {
                            case OK_VALID_USER:
                                Toasty.success(getApplicationContext(), "Success!", Toast.LENGTH_SHORT, true).show();
                                Intent intent = new Intent(SignInActivity.this, MenuActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case  NO_VALID_USER:
                                Toasty.error(getApplicationContext(), "Error. Uncorrect login or password!", Toast.LENGTH_SHORT, true).show();
                                break;
                            case VALID_USER_EXIST:
                                Toasty.info(getApplicationContext(), "User exist!", Toast.LENGTH_SHORT, true).show();
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toasty.warning(getApplicationContext(), "Error, your login or password is empty!", Toast.LENGTH_SHORT, true).show();
                }
                mRunLoad.hide();
            }
        });

        mSignUpButton = (Button)findViewById(R.id.create_account_button);
        mSignUpButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }
/*    private void ChangeActivity(Context from, Context to) {
        Intent intent = new Intent(from, to.getClass());
        startActivity(intent);
        finish();
    }*/
    private class AuthAsyncTask extends AsyncTask<URL, Void, User> {
        @Override
        protected User doInBackground(URL... urls) {
            // Create URL object
            URL url = HttpRequest.createUrl(UserUtils.Login.URL + "/"
                    + mUser.getLoginText() + "/" + mUser.getPasswordText());
            // Perform HTTP request to the URL and receive a JSON response back
            String jsonResponse = "";
            try {
                jsonResponse = HttpRequest.makeHttpRequest(url, "GET");
            } catch (IOException e) {
                Log.d("AsyncTask", "Response error");
                // TODO Handle the IOException
            }
            if (jsonResponse.equals("user_exist")) {
                mUser.setValid(UserUtils.Login.OK_VALID_USER);
            } else {

                mUser.setValid(UserUtils.Login.NO_VALID_USER);
            }
            // Return the {@link Event} object as the result fo the {@link TsunamiAsyncTask}
            return mUser;
        }
    }
    @SuppressLint({"ResourceAsColor", "ResourceType"})
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v != mLoginEditText) {
            mLoginEditText.setHintTextColor(R.color.colorBlack);
        } else {
            if (v != mPasswordEditText) {
                mPasswordEditText.setHintTextColor(R.color.colorBlack);
            }
        }
    }
}

