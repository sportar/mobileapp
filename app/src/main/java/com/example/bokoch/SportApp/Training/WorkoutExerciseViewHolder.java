package com.example.bokoch.SportApp.Training;


import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.example.bokoch.SportApp.R;

import es.dmoral.toasty.Toasty;

public class WorkoutExerciseViewHolder extends ChildViewHolder implements View.OnClickListener {
    AlertDialog.Builder alertDialog;
    WorkoutExercise mWorkoutExercise;
    private TextView mWorkoutExerciseTextView;
    private Context mContext;
    public WorkoutExerciseViewHolder(@NonNull View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        Log.d("Workout_exercise holder", "Init");
        mWorkoutExerciseTextView = (TextView) itemView.findViewById(R.id.workout_exercise_textview);
        itemView.setOnClickListener(this);
    }

    public void bind(@NonNull WorkoutExercise workoutExercise) {
        Log.d("Workout_exercise holder", "setText");
        mWorkoutExercise = workoutExercise;
        mWorkoutExerciseTextView.setText(workoutExercise.getName());
    }
    @Override
    public void onClick(View v) {
//        FragmentManager fm = v.getContext().getS
//        FragmentDialogWorkoutExercise dialog = new FragmentDialogWorkoutExercise();
//        dialog.show();
        Toasty.warning(v.getContext(), "fdsfsfsf").show();

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(v.getContext());
//        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle(mWorkoutExercise.getName());

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(v.getContext(), android.R.layout.simple_expandable_list_item_1);
        for (int i = 0; i < mWorkoutExercise.getSets().length; i++) {
            arrayAdapter.add(String.valueOf(i) + ". " + mWorkoutExercise.getSets()[i]);
        }

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(mContext.getApplicationContext());
                builderInner.setMessage(strName);
                builderInner.setTitle("Your Selected Item is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });

            }
        });
        builderSingle.show();
    }
}