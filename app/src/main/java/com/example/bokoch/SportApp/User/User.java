package com.example.bokoch.SportApp.User;

/**
 * Created by bokoch on 2/24/2018.
 */

public class User {

    private String loginText;
    private String passwordText;
    private String mailText;
    private String mId;
    private int mValid;
    public User() {

    }

    public int getValid() {
        return mValid;
    }

    public void setValid(int valid) {
        this.mValid = valid;
    }


    public User(String ltext, String ptext, String id) {
        this.setLoginText(ltext);
        this.setPasswordText(ptext);
        this.setId(id);
    }
    public User(String ltext, String ptext, String mText, String id) {
        this.setLoginText(ltext);
        this.setPasswordText(ptext);
        this.setMailText(mText);
        this.setId(id);
    }
    public void setMailText(String mailText) { this.mailText = mailText; }

    public String getMailText() { return mailText; }

    public String getPasswordText() {
        return passwordText;
    }

    public final void setPasswordText(String text) {
        passwordText = text;
    }

    public String getLoginText() {
        return loginText;
    }

    public final void setLoginText(String text) {
        loginText = text;
    }

    public String getId() {
        return mId;
    }

    public final void setId(String id) {
        mId = id;
    }
}
