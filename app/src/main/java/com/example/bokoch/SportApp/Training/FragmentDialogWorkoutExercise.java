package com.example.bokoch.SportApp.Training;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.bokoch.SportApp.R;

import java.util.List;

public class FragmentDialogWorkoutExercise extends DialogFragment implements
                        AdapterView.OnItemClickListener {

    WorkoutExercise mWorkoutExercise;
    ListView mylist;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.workout_exercise_sets_view, null, false);
        mylist = (ListView) view.findViewById(R.id.list);

        getDialog().setTitle(mWorkoutExercise.getName());
        return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {

            super.onActivityCreated(savedInstanceState);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
            android.R.layout.simple_list_item_1, mWorkoutExercise.getSets().length);

            mylist.setAdapter(adapter);

            mylist.setOnItemClickListener(this);

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            dismiss();
            Toast.makeText(getActivity(), String.valueOf(position) + ". " + mWorkoutExercise.getSets()[position], Toast.LENGTH_SHORT)
            .show();
        }
        /*private class AdapterWorkoutExercise extends RecyclerView.Adapter<WorkoutExercise> {
            private List<String> mSets;
            public AdapterWorkoutExercise(List<String> sets) {
                mSets = sets;
            }
        }*/
}