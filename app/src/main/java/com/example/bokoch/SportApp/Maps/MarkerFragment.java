package com.example.bokoch.SportApp.Maps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
;
import com.example.bokoch.SportApp.Maps.Markers.MarkerLab;
import com.example.bokoch.SportApp.Maps.Markers.MarkerMaps;
import com.example.bokoch.SportApp.R;

public class MarkerFragment extends Fragment {
    public static final String ARG_MARKER_NAME = "marker_name";
    public static final String ARG_MARKER_DISTANCE = "marker_distance";
    public static final String ARG_MARKER_CAPTURE = "marker_capture";

    private String mName;
    private Double mDistance;

    private TextView mMarkerName;
    private TextView mMarkerDistance;



    public static MarkerFragment newInstance(Bundle args) {
        MarkerFragment fragment = new MarkerFragment();
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        int MarkerId = (int)getArguments().getSerializable(ARG_MARKER_ID);
//        Log.d("ExerciseOnCreate", String.valueOf(MarkerId));
//        mMarkerMaps = MarkerLab.get(getActivity()).getMarker(MarkerId);
//
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        mName = getArguments().getString(ARG_MARKER_NAME);
        mDistance = getArguments().getDouble(ARG_MARKER_DISTANCE);
        View v = inflater.inflate(R.layout.fragment_marker, container, false);

        mMarkerName = (TextView)v.findViewById(R.id.name_marker_text_view);
        mMarkerDistance = (TextView)v.findViewById(R.id.distance_text_view);

        mMarkerName.setText(mName);
        mMarkerDistance.setText(String.valueOf(mDistance));
        return v;
    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
    }
    public void returnResult() {
        getActivity().setResult(Activity.RESULT_OK, null);
    }
}
