package com.example.bokoch.SportApp.Training.Utils;


public class WorkoutsRequest {
    public static final String WORKOUT_ID = "workoutId";
    public static final String WORKOUT_DATE = "workoutDateTime";
    public static final String WORKOUT_EXERCISES = "exercises";
    public static final String WORKOUTS_EXAMPLES = "[\n" +
            "    {\n" +
            "        \"workoutId\": 0,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"Push Ups\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 1,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"Push Ups\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 2,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"sosat biba\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 3,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"prosed on butilka\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 4,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"Push Ups\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 5,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"sosat biba\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    },\n" +
            "    {\n" +
            "        \"workoutId\": 6,\n" +
            "        \"workoutDateTime\": \"2018-03-16\",\n" +
            "        \"exercises\": [\n" +
            "            {\n" +
            "                \"exerciseId\": 0,\n" +
            "                \"name\": \"prosed on butilka\",\n" +
            "                \"weight\": 89,\n" +
            "                \"sets\": [\n" +
            "                    100,\n" +
            "                    200,\n" +
            "                    300\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"exerciseId\": 1,\n" +
            "                \"name\": \"test\",\n" +
            "                \"weight\": 0,\n" +
            "                \"sets\": [\n" +
            "                    1,\n" +
            "                    2,\n" +
            "                    3\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "]";
    public final class WorkoutExercise {
        public static final String EXERCISE_ID = "exerciseId";
        public static final String EXERCISE_NAME = "name";
        public static final String EXERCISE_WEIGHT = "weight";
        public static final String EXERCISE_SETS = "sets";

    }

}
/*[
    {
        "workoutId": 0,
        "workoutDateTime": "2018-03-16T20:47:17.41Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "Push Ups",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 1,
        "workoutDateTime": "2018-03-16T20:49:43.639Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "Push Ups",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 2,
        "workoutDateTime": "2018-03-16T20:49:43.712Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "sosat biba",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 3,
        "workoutDateTime": "2018-03-16T20:49:43.816Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "prosed on butilka",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 4,
        "workoutDateTime": "2018-03-16T20:59:55.169Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "Push Ups",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 5,
        "workoutDateTime": "2018-03-16T20:59:55.306Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "sosat biba",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            }
        ]
    },
    {
        "workoutId": 6,
        "workoutDateTime": "2018-03-16T20:59:55.31Z",
        "exercises": [
            {
                "exerciseId": 0,
                "name": "prosed on butilka",
                "weight": 89,
                "sets": [
                    100,
                    200,
                    300
                ]
            },
            {
                "exerciseId": 1,
                "name": "test",
                "weight": 0,
                "sets": [
                    1,
                    2,
                    3
                ]
            }
        ]
    }
]*/