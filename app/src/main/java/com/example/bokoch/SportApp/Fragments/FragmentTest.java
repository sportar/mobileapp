package com.example.bokoch.SportApp.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.Testing.TestUtils;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class FragmentTest extends Fragment {
    private static final int REQUEST_Skill = 1;

    String[] mSkills;
    private RecyclerView mSkillRecyclerView;
    private FragmentTest.SkillAdapter mAdapter;
    // Фабрика для создания фрагмента
    public static FragmentTest newInstance() {
        FragmentTest fragment = new FragmentTest();
        Log.i("newInstance", "Created");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("SkillListView", "onCreate");

    }

    //Перегрузка списка
    @Override
    public void onResume() {
        super.onResume();
        UpdateUI();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        mSkillRecyclerView = (RecyclerView)view.findViewById(R.id.list_view_exercise);
        mSkillRecyclerView.setHasFixedSize(true);
        mSkillRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Log.d("SkillListView", "onCreateView");
        UpdateUI();
        return view;
    }
    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        Log.i("OnAcrivityCreated", "Error");
    }

    //обновление фрагмента
    private void UpdateUI() {
        mSkills = TestUtils.RecyclerViewSkill;
        if (mAdapter == null) {
            mAdapter = new FragmentTest.SkillAdapter(mSkills);

        } else {
            mAdapter.setSkills(mSkills);

            mAdapter.notifyDataSetChanged();
        }
        mSkillRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("Detach", "Detach");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void returnResult() {
        getActivity().setResult(Activity.RESULT_OK, null);
    }

    public class SkillHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitleNameTextView;

        public SkillHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleNameTextView = (TextView) itemView.findViewById(R.id.text_name_skills);
        }
        @Override
        public void onClick(View v) {
            Toasty.normal(getActivity(), "dsfdf", Toast.LENGTH_SHORT);
//            Intent intent = mSkillRecyclerView.newIntent(getActivity(), mSkill);
//            startActivityForResult(intent, REQUEST_Skill);

        }


        public void bindSkill(String mSkill) {
            mTitleNameTextView.setText(mSkill);
        }
    }

//    @Override
//    public void onActivityResult(int requesCode, int resultCode, Intent data) {
//        if (requesCode == REQUEST_CRIME) {
//
//        }
//    }


    private class SkillAdapter extends RecyclerView.Adapter<FragmentTest.SkillHolder> {
        private String[]  mSkills;
        public SkillAdapter(String[] Skills) {
            mSkills = Skills;
        }
        @Override
        public FragmentTest.SkillHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.test_skill_recycler_view_item, parent, false);
            return new FragmentTest.SkillHolder(view);
        }
        public void setSkills(String[] Skills) {
            mSkills = Skills;
        }
        @Override
        public void onBindViewHolder(FragmentTest.SkillHolder holder, int position) {
            Log.i("OnBindViewHolder", "Position = " + position);
//            String mSkill = mSkills[position];
            holder.bindSkill(mSkills[position]);
        }
        @Override
        public int getItemCount() {
            return mSkills.length;
        }

    }
}