package com.example.bokoch.SportApp.DataBase;



public class DbSchema {
    public static String DATA_BASE_NAME = "exercise_library_item.db";
    public static final class ExerciseTable {
        public static final String NAME = "exercises";

        public static final class Cols {
            public static final String UUID = "id";
            public static final String NAME_EXERCISE = "class";
            public static final String TYPE = "type";
            public static final String COMPLEXITY = "complexity";
            public static final String DESCRIPTION = "description";
            public static final String IMAGE = "image";


        }
    }

    public static final class MarkersTable {
        public static final String NAME = "markers";

        public static final class Cols {
            public static final String ID = "id";
            public static final String NAME_PLACES = "name";
            public static final String LAT = "lat";
            public static final String LNG = "lng";


        }
    }
    public static final class WorkoutTable {
        public static final String NAME = "workouts";

        public static final class Cols {
            public static final String ID = "id";
            public static final String DATE_TRAINING = "date";
            public static final String LIST_EXERCISE = "list_exercise";
        }
    }


}