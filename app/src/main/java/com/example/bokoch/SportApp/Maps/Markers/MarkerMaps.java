package com.example.bokoch.SportApp.Maps.Markers;


import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.json.JSONObject;

public class MarkerMaps implements ClusterItem {
    private final LatLng mPosition;
    private String mNamePlaces;
    private int mId;

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    private double mDistance;

    public MarkerMaps(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public MarkerMaps(int id, String namePlaces, double lat, double lng) {
        mPosition = new LatLng(lat, lng);
        mNamePlaces = namePlaces;
        mId = id;
    }

    public String getNamePlaces() {
        return mNamePlaces;
    }

    public void setNamePlaces(String namePlaces) {
        mNamePlaces = namePlaces;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

}
