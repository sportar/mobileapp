package com.example.bokoch.SportApp.Fragments;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bokoch.SportApp.ExerciseLibraryPagerActivity;
import com.example.bokoch.SportApp.Exercises.Exercise;
import com.example.bokoch.SportApp.Exercises.ExerciseLab;
import com.example.bokoch.SportApp.R;

import java.util.List;


public class ExerciseLibraryRecyclerView extends Fragment {
    private static final int REQUEST_EXERCISE = 1;

    private RecyclerView mExerciseRecyclerView;
    private ExerciseAdapter mAdapter;
    // Фабрика для создания фрагмента
    public static ExerciseLibraryRecyclerView newInstance() {
        ExerciseLibraryRecyclerView fragment = new ExerciseLibraryRecyclerView();
        Log.i("newInstance", "Created");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ExerciseListView", "onCreate");
       /* if (savedInstanceState != null) {
            //Restore the fragment instance
            mContent = getFragmentManager().getFragment(savedInstanceState, "myFragmentName");
        }*/
    }

    //Перегрузка списка
    @Override
    public void onResume() {
        super.onResume();
        UpdateUI();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment instance
        getFragmentManager().putFragment(outState, "myFragmentName", mContent);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_exercise_library, container, false);
        mExerciseRecyclerView = (RecyclerView)view.findViewById(R.id.list_view_exercise);
        mExerciseRecyclerView.setHasFixedSize(true);
        mExerciseRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Log.d("ExerciseListView", "onCreateView");
        UpdateUI();
        return view;
    }
    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        Log.i("OnAcrivityCreated", "Error");
    }

    //обновление фрагмента
    private void UpdateUI() {
        ExerciseLab exerciseLab = ExerciseLab.get(getActivity());
        List<Exercise> exercises = exerciseLab.getExercises();
        if (mAdapter == null) {
            mAdapter = new ExerciseAdapter(exercises);

        } else {
            mAdapter.setExercises(exercises);

            mAdapter.notifyDataSetChanged();
        }
        mExerciseRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("Detach", "Detach");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        Log.i("DestroyView", "Detach");
//        savingListExercise = new LinkedList();
//        for (int i = 0; i < mAdapter.getItemCount(); i++) {
//            savingListExercise.add(mAdapter.getItemId(i));
//        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void returnResult() {
        getActivity().setResult(Activity.RESULT_OK, null);
    }

    public class ExerciseHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitleNameTextView;
        private TextView mTitleTypeTextView;
        private ImageView mTitleComplexityImageView;
        private Exercise mExercise;
        private ImageView mExerciseImageView;

        public ExerciseHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mExerciseImageView = (ImageView) itemView.findViewById(R.id.exercise_image_view_item);
            mTitleNameTextView = (TextView) itemView.findViewById(R.id.text_name_exercise);
            mTitleTypeTextView = (TextView) itemView.findViewById(R.id.text_type_exercise);
            mTitleComplexityImageView = (ImageView) itemView.findViewById(R.id.complexity_image_view);
        }
        @Override
        public void onClick(View v) {

            Intent intent = ExerciseLibraryPagerActivity.newIntent(getActivity(), mExercise.getID());
            startActivityForResult(intent, REQUEST_EXERCISE);
            /*final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.exercise_library_item);
            dialog.setTitle(mExercise.getName());
            dialog.setCancelable(true); // dismiss when touching outside Dialog

            // set the custom dialog components - texts and image
            TextView mDescription = dialog.findViewById(R.id.text_type_exercise);
            TextView mType = (TextView) dialog.findViewById(R.id.text_name_exercise);
            ImageView icon = (ImageView) dialog.findViewById(R.id.complexity_image_view);
            mType.setText(mExercise.getType());
            mDescription.setText(mExercise.getDescription());
            icon.setImageResource(mExercise.getImage());
            dialog.show();*/
//            Toast.makeText(getActivity(), mExercise.getName()+" clicked!", Toast.LENGTH_SHORT).show();
        }


        public void bindExercise(Exercise exercise) {
            Log.i("bindExercise()", "exerciseSetimageId");
            mExercise = exercise;
            switch (mExercise.getID()) {
                case 1:
                    mExerciseImageView.setImageResource(R.drawable.push_up);
                    break;
                case 2:
                    mExerciseImageView.setImageResource(R.drawable.birpi);
                    break;
                case 3:
                    mExerciseImageView.setImageResource(R.drawable.prisedania);
                    break;
                default:
                    mExerciseImageView.setImageResource(R.drawable.birpi);
                    Log.i("EveryWheree", String.valueOf(mExercise.getID()));
                    break;

            }
            mTitleNameTextView.setText(mExercise.getName().toString());
            mTitleTypeTextView.setText(mExercise.getType().toString());
            mTitleComplexityImageView.setImageResource(mExercise.getComplexityImage());

        }
    }

//    @Override
//    public void onActivityResult(int requesCode, int resultCode, Intent data) {
//        if (requesCode == REQUEST_CRIME) {
//
//        }
//    }


    private class ExerciseAdapter extends RecyclerView.Adapter<ExerciseHolder> {
        private List<Exercise> mExercises;
        public ExerciseAdapter(List<Exercise> exercises) {
            mExercises = exercises;
        }
        @Override
        public ExerciseHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.exercise_library_item, parent, false);
            return new ExerciseHolder(view);
        }
        public void setExercises(List<Exercise> exercises) {
            mExercises = exercises;
        }
        @Override
        public void onBindViewHolder(ExerciseHolder holder, int position) {
            Log.i("OnBindViewHolder", "Position = " + position);
            Exercise exercise = mExercises.get(position);
            holder.bindExercise(exercise);
//            mAdapter.notifyDataSetChanged();
            //make update one record: mAdapter.notifyDataSetChanged();
        }
        @Override
        public int getItemCount() {
            return mExercises.size();
        }

    }
}


