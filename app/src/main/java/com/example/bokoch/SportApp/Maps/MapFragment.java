package com.example.bokoch.SportApp.Maps;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bokoch.SportApp.Maps.Markers.MarkerLab;
import com.example.bokoch.SportApp.Maps.Markers.MarkerMaps;
import com.example.bokoch.SportApp.Maps.Markers.MarkerRenderer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

import static com.example.bokoch.SportApp.R.*;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private int MAX_DISTANCE = 2000;

    private ClusterManager<MarkerMaps> mClusterManager;
    private GoogleMap mMap;
    private List<MarkerMaps> mMarkers;
    private LatLng mUserLocation;

    ViewPager mViewPager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mMarkers = new ArrayList<>();
        View view = inflater.inflate(layout.activity_maps_fragment, container, false);
        UpdateUI();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(id.map);
        mapFragment.getMapAsync(this);

        mViewPager = (ViewPager) view.findViewById(id.marker_view_pager);
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mMarkers));
        return view;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    //обновление фрагмента
    private void UpdateUI() {
        MarkerLab markerLab = MarkerLab.get(getActivity());
        mMarkers = markerLab.getMarkers();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        UpdateUI();
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            Log.e("DB", "PERMISSION GRANTED");
        }

        mMap.setMyLocationEnabled(true);
        LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(criteria, true);
            myLocation = lm.getLastKnownLocation(provider);
        }

        if (myLocation != null) {

            mUserLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mUserLocation, 14), 1500, null);
        } else {
            return;
        }

        mUserLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

        setUpClusterer();

    }

    public static double CalculationDistance(LatLng StartP, LatLng EndP) {
        double distance = CalculationDistanceByCoord(StartP.latitude, StartP.longitude, EndP.latitude, EndP.longitude);
        return distance;
    }

    private static double CalculationDistanceByCoord(double startPointLat,double startPointLon,double endPointLat,double endPointLon){
        float[] results = new float[1];
        Location.distanceBetween(startPointLat, startPointLon, endPointLat, endPointLon, results);
        return results[0];
    }

    private void setUpClusterer() {
        mClusterManager = new ClusterManager<MarkerMaps>(getContext(), mMap);
        //закомментить
        MarkerRenderer customRenderer = new MarkerRenderer(getActivity(), mClusterManager, mMap);
        mClusterManager.setRenderer(customRenderer);
        //закомментить

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {

        for(int marker = 0; marker < mMarkers.size(); marker++) {
            LatLng place = mMarkers.get(marker).getPosition();
            double distance = CalculationDistance(place, mUserLocation);
            if (distance > MAX_DISTANCE || distance == 0.0) {
                mMarkers.remove(marker);
            } else {
                mMarkers.get(marker).setDistance(distance);
            }
        }
        mClusterManager.addItems(mMarkers);
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mMarkers));
    }

}