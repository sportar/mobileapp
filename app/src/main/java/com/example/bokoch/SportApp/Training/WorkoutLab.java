package com.example.bokoch.SportApp.Training;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.bokoch.SportApp.DataBase.DataBase;
import com.example.bokoch.SportApp.DataBase.DataBaseCursorWrapper;
import com.example.bokoch.SportApp.DataBase.DbSchema;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class WorkoutLab {
    /*private static WorkoutLab sWorkoutLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DataBase mBaseHelper;

    public void addWorkout(Workout c) {
        ContentValues values = getContentValues(c);
    }

    public void updateWorkout(Workout Workout) {
        String uuidString = String.valueOf(Workout.getId());
        ContentValues values = getContentValues(Workout);

        mDatabase.update(DbSchema.WorkoutTable.NAME, values,
                DbSchema.WorkoutTable.Cols.ID + " = ?",
                new String[] {uuidString});

    }

    private DataBaseCursorWrapper queryWorkout(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                DbSchema.WorkoutTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new DataBaseCursorWrapper(cursor);
    }

    public static WorkoutLab get(Context context) {
        if (sWorkoutLab == null)
            sWorkoutLab = new WorkoutLab(context);
        return sWorkoutLab;
    }
    private WorkoutLab(Context context) {
        mContext = context.getApplicationContext();

        Log.d("Workout lab", "GETBASEHELPER");
        mBaseHelper = new DataBase(context);
        try {
            mBaseHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDatabase = mBaseHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }


    public List<Workout> getWorkouts() {
        List<Workout> workout = new ArrayList<>();
        Cursor c = mDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Log.d("Table Name=> ", c.getString(0));
                c.moveToNext();
            }
        }
        DataBaseCursorWrapper cursor = queryWorkout(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                workout.add(cursor.getWorkout());
                cursor.moveToNext();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return workout;
    }
    private static ContentValues getContentValues(Workout Workout) {
        ContentValues values = new ContentValues();
        values.put(DbSchema.WorkoutTable.Cols.ID, String.valueOf(Workout.getId()));
        values.put(DbSchema.WorkoutTable.Cols.DATE_TRAINING, Workout.getDateString());
        values.put(DbSchema.WorkoutTable.Cols.LIST_EXERCISE, Workout.getTrainingString());
        return values;
    }
    public Workout getWorkout(int id) {
        DataBaseCursorWrapper cursor = queryWorkout(DbSchema.WorkoutTable.Cols.ID + " = ?",
                new String[] { String.valueOf(id) });
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getWorkout();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return null;
    }
*/
}
