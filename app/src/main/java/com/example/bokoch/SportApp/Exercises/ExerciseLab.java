package com.example.bokoch.SportApp.Exercises;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.bokoch.SportApp.DataBase.DataBase;
import com.example.bokoch.SportApp.DataBase.DataBaseCursorWrapper;
import com.example.bokoch.SportApp.DataBase.DbSchema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExerciseLab {

    private static ExerciseLab sExerciseLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DataBase mBaseHelper;

    public void addExercise(Exercise c) {
        ContentValues values = getContentValues(c);
    }

    public void updateExercise(Exercise exercise) {
        String uuidString = String.valueOf(exercise.getID());
        ContentValues values = getContentValues(exercise);

        mDatabase.update(DbSchema.ExerciseTable.NAME, values,
                DbSchema.ExerciseTable.Cols.UUID + " = ?",
                new String[] {uuidString});

    }

    private DataBaseCursorWrapper queryExercise(String whereClause, String[] whereArgs) {
         Cursor cursor = mDatabase.query(
                DbSchema.ExerciseTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new DataBaseCursorWrapper(cursor);
    }

    public static ExerciseLab get(Context context) {
        if (sExerciseLab == null)
            sExerciseLab = new ExerciseLab(context);
        return sExerciseLab;
    }
    private ExerciseLab(Context context) {
        mContext = context.getApplicationContext();
        mBaseHelper = new DataBase(context);
        try {
            mBaseHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDatabase = mBaseHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }


    public List<Exercise> getExercises() {
        List<Exercise> crimes = new ArrayList<>();
        DataBaseCursorWrapper cursor = queryExercise(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                crimes.add(cursor.getExercise());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return crimes;
    }
    private static ContentValues getContentValues(Exercise exercise) {
        ContentValues values = new ContentValues();
        values.put(DbSchema.ExerciseTable.Cols.UUID, String.valueOf(exercise.getID()));
        values.put(DbSchema.ExerciseTable.Cols.DESCRIPTION, exercise.getDescription());
        values.put(DbSchema.ExerciseTable.Cols.TYPE, exercise.getType());
        values.put(DbSchema.ExerciseTable.Cols.NAME_EXERCISE, exercise.getName());
        values.put(DbSchema.ExerciseTable.Cols.COMPLEXITY, exercise.getComplexity());
        return values;
    }
    public Exercise getExercise(int id) {
        DataBaseCursorWrapper cursor = queryExercise(DbSchema.ExerciseTable.Cols.UUID + " = ?",
                new String[] { String.valueOf(id) });
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getExercise();
        } finally {
            cursor.close();
        }
    }

}
