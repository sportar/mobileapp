package com.example.bokoch.SportApp.DataBase;

import android.content.Context;
import android.util.Log;

import com.example.bokoch.SportApp.Maps.Markers.MarkerMaps;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.Training.Utils.WorkoutsRequest;
import com.example.bokoch.SportApp.Training.Workout;
import com.example.bokoch.SportApp.Training.WorkoutExercise;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class JsonParser {


//    public static List<MarkerMaps> readMarkersJSONFile(Context context) throws
//                                        IOException, JSONException {
//
//        String jsonText = readText(context, R.raw.markers);
//        List<MarkerMaps> markers = new ArrayList<>();
//        JSONArray jsonRoot = new JSONArray(jsonText);
//            for (int i = 0; i < jsonRoot.length(); i++) {
//                JSONObject marker = jsonRoot.getJSONObject(i);
//                markers.add(new MarkerMaps(marker));
//            }
//        return markers;
//    }



    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while((s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    public static List<Workout> getWorkoutFromJSON(String objectJson) throws JSONException, ParseException {
        JSONArray workoutsUser = new JSONArray(objectJson);
        List<Workout> workouts = new ArrayList<>();
        Log.d("Workouts = ", String.valueOf(workoutsUser.length()));
        //по каждой тренировке
        for (int i = 0; i < workoutsUser.length(); i++) {

            JSONObject workoutJson = workoutsUser.getJSONObject(i);
            int id = workoutJson.getInt(WorkoutsRequest.WORKOUT_ID);
            Log.i("id", String.valueOf(id));
            String dateStr = workoutJson.getString(WorkoutsRequest.WORKOUT_DATE);
            Log.i("dateStr", dateStr);

            Date date = ConvertDateFromString(dateStr);
            JSONArray exercisesJson = workoutJson.getJSONArray(WorkoutsRequest.WORKOUT_EXERCISES);
            List<WorkoutExercise> workoutExercises = new ArrayList<>();
            Log.d("Workout = ", String.valueOf(exercisesJson.length()));

            for (int j = 0; j < exercisesJson.length(); j++){
                JSONObject exerciseJSON = exercisesJson.getJSONObject(j);

                int exerciseId = exerciseJSON.getInt(WorkoutsRequest.WorkoutExercise.EXERCISE_ID);
                String exerciseName = exerciseJSON.getString(WorkoutsRequest.WorkoutExercise.EXERCISE_NAME);
                int exerciseWeight = exerciseJSON.getInt(WorkoutsRequest.WorkoutExercise.EXERCISE_WEIGHT);
                Log.i("exerciseName", exerciseName);

                JSONArray setsJSON = exerciseJSON.getJSONArray(WorkoutsRequest.WorkoutExercise.EXERCISE_SETS);
                String[] workoutSets = new String[setsJSON.length()];
                for (int k = 0; k < setsJSON.length(); k++) {
                    workoutSets[k] = setsJSON.getString(k);
                }
                workoutExercises.add(new WorkoutExercise(exerciseId, exerciseName, exerciseWeight, workoutSets));
            }

            workouts.add(new Workout(id, date, workoutExercises));

        }

        return workouts;
    }
    private static Date ConvertDateFromString(String st) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return formatter.parse(st);
    }
}
