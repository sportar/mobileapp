package com.example.bokoch.SportApp.Menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.bokoch.SportApp.Fragments.FragmentDiaryTraining;
import com.example.bokoch.SportApp.Fragments.ExerciseLibraryRecyclerView;
import com.example.bokoch.SportApp.Fragments.FragmentProgress;
import com.example.bokoch.SportApp.Fragments.FragmentTest;
import com.example.bokoch.SportApp.Maps.MapFragment;
import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.LoginActivity.SessionManager;
import com.example.bokoch.SportApp.LoginActivity.SignInActivity;
import com.example.bokoch.SportApp.Training.FragmentWorkoutsListRecyclerView;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ExerciseLibraryRecyclerView fExerxises;
    FragmentDiaryTraining fDiaryTraining;
    FragmentProgress fProgres;
    FragmentTest fTest;
    MapFragment fMaps;
    FragmentWorkoutsListRecyclerView fWorkout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fWorkout = new FragmentWorkoutsListRecyclerView();
        fTest = new FragmentTest();
        fExerxises = new ExerciseLibraryRecyclerView();
        fDiaryTraining = new FragmentDiaryTraining();
        fProgres = new FragmentProgress();
        fMaps = new MapFragment();
    }

    /*@Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        FloatingActionButtonAddTraining fabButton = new FloatingActionButtonAddTraining.Builder(this)
                .withDrawable(getResources().getDrawable(R.drawable.floating_action))
                .withButtonColor(Color.WHITE)
                .withGravity(Gravity.BOTTOM | Gravity.RIGHT)
                .withMargins(0, 0, 16, 16)
                .create();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.create_training_item) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.succesful_training) {
            fragmentManager.beginTransaction().replace(R.id.progress_fragment, fWorkout).addToBackStack(null).commit();
        } else if (id == R.id.maps_places_item) {
            fragmentManager.beginTransaction().replace(R.id.progress_fragment, fMaps).addToBackStack(null).commit();
        } else if (id == R.id.list_exercises_item) {
            fragmentManager.beginTransaction().replace(R.id.progress_fragment, fExerxises).addToBackStack(null).commit();
        } else if (id == R.id.test_training_item) {
            fragmentManager.beginTransaction().replace(R.id.progress_fragment, fTest).addToBackStack(null).commit();
        } else if (id == R.id.training_diary_item) {
            fragmentManager.beginTransaction().replace(R.id.progress_fragment, fDiaryTraining).addToBackStack(null).commit();
        } else if (id == R.id.logout_item) {
            BackToAuthenfication(getApplicationContext());
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);

        return super.onOptionsItemSelected(item);
    }

    public void BackToAuthenfication(Context context) {
        new SessionManager(getApplicationContext()).removeLogin();
        //Пофиксить
        Intent intent = new Intent(context, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0)
            this.finish();
    }
}

