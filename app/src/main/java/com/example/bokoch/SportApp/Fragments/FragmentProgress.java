package com.example.bokoch.SportApp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bokoch.SportApp.R;
import com.example.bokoch.SportApp.LoginActivity.SessionManager;


public class FragmentProgress extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.progress_fragment,
                container, false);

        return view;
    }
}